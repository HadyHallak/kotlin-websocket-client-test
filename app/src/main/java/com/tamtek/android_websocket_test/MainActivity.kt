package com.tamtek.android_websocket_test

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import okhttp3.*


class MainActivity : AppCompatActivity() {

    private val client = OkHttpClient()

    private var ws: WebSocket? = null

    private var logTextView: TextView? = null
    private var editTextMessage: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        logTextView = findViewById(R.id.output)
        editTextMessage = findViewById(R.id.editTextMessage)

        findViewById<Button>(R.id.buttonSend).setOnClickListener { view ->
            val message = editTextMessage?.text.toString()
            if (!message.isEmpty()) {
                ws?.send(message)
                editTextMessage?.setText("")
            }
        }

        connect()
    }

    fun connect() {
        val request = Request.Builder()
            .url("wss://websockettestproject20181220045804.azurewebsites.net/chat")
            .build()

        val listener = EchoWebSocketListener()
        ws = client.newWebSocket(request, listener)
        client.dispatcher().executorService().shutdown()
    }

    fun output(msg: String) {
        runOnUiThread { logTextView!!.text = logTextView!!.text.toString() + "\n\n" + msg }
    }


    inner class EchoWebSocketListener: WebSocketListener() {

        override fun onOpen(webSocket: WebSocket, response: Response) {
            output("Connected to socket successfully.")
        }

        override fun onMessage(webSocket: WebSocket, text: String) {
            output(text)
        }


        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            output("Connection closed, Reason" + reason)
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            output("Connection failed, Reason: " + response?.message())
        }

    }
}

